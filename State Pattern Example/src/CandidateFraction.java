/**
 * A candidate implementation for the Fraction interface.  Sadly this
 * contains errors.  
 * @author gosnat
 * @author Tyler Miller
 *
 */
public class CandidateFraction implements Fraction {
	/** Numerator of the fraction */
	private int numerator;
	
	/** Denominator of the fraction */
	private int denominator;
	
	/**
	 * Creates and reduces a fraction
	 * @param numerator starting numerator
	 * @param denominator starting denominator
	 * @throws ArithmeticException if denominator is zero
	 */
	public CandidateFraction(int numerator, int denominator) throws ArithmeticException {
		if(denominator == 0) throw new ArithmeticException("Denominator can't be zero");
		this.denominator = denominator;
		this.numerator = numerator;
		this.reduce();
	}

	@Override
	public int getNumerator() {
		return numerator;
	}

	@Override
	public int getDenominator() {
		return denominator;
	}
	
	@Override
	public Fraction add(Fraction other) throws ArithmeticException{
		long tempNum1 = this.numerator;
		long tempNum2 = other.getNumerator();
		long tempDen = this.denominator;
		tempNum1 *= other.getDenominator();
		tempNum2 *= this.denominator;
		tempDen *= other.getDenominator();
		
		long gcd = gcd(Math.abs(tempNum1+tempNum2), Math.abs(tempDen));
		long numerator = (tempNum1+tempNum2) / gcd;
		long denominator = tempDen / gcd;
		
		if(denominator > Integer.MAX_VALUE) {
			throw new ArithmeticException("Denominator Overflow");
		}
		if(numerator > Integer.MAX_VALUE) {
			throw new ArithmeticException("Numerator Overflow");
		}
				
		CandidateFraction answer = new CandidateFraction((int) numerator, (int) denominator);
		
		return answer;
	}
	
	/**
	 * Convert fraction to reduced form
	 */
	private void reduce() {
		// Reduce zero fractions to 0/1
		if (numerator == 0) {
			denominator = 1;
		} else {
			//find GCD
			long a = Math.abs(numerator);
			long b = Math.abs(denominator);
			long gcd = gcd(a,b);

			numerator /= gcd;
			denominator /= gcd;
		}
	    
	    // +/- become -/+ and -/- become +/+
		if (denominator < 0) {
			denominator *= -1;
			numerator *= -1;
		}
	}
	
	/** Returns the GCD of two numbers */
	private long gcd(long a, long b) {
		while (a != b) {
			if (a > b) {
				a = a - b;
			} else {
				b = b - a;
			}
		}
		return a;
	}

}